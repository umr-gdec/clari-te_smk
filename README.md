# CLARI-TE_smk - A snakemake pipeline for Transposable Element annotation  

CLARI-TE_smk is a snakemake pipeline for genome wide annotation of Transposable Elements (TEs) of *Triticeae* genomes based on RepeatMasker and CLARI-TE ([Daron et al, 2014](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-014-0546-4)).  
This Snakemake workflow is developped to be run on a High Performance Computing (HPC) cluster with slurm as cluster management and job scheduling system.
It automatically performs distributed calculs and thus annotates TEs of different chromosomes of a genome at the same time. At the end it creates a gff3 annotation file.  

## Requirements  

To run CLARI-TE_smk, it requires a Linux HPC cluster (slurm), Singularity, Python, and Snakemake.

Dependancies (contained in Singularity image):  
>  - samtools/1.16
>  - bedtools/2.27
>  - exonerate/2.4
>  - genometools-genometools/1.5
>  - RepeatMasker/4.0
>  - perl-bioperl/1.7
>  - perl-getopt-long
>  - perl-data-dumper
>  - clariTE_1.0
>  - cross_match/1.090518

## Installation  

Download the pipeline:

```console
git clone https://forgemia.inra.fr/umr-gdec/clari-te_smk.git
```

Download the Singularity image:  

```console
singularity pull oras://registry.forgemia.inra.fr/umr-gdec/clari-te_smk:latest
```

## Setting up  

Inputs are the genome fasta file, and CLARI-TE_smk files whose 2 files (eventually 3) have to be completed by user.

Fill in the "smk_config.yaml" file with genome informations (genome fasta path, TEs features prefix, chromosomes list).  
Custom the cluster parameters set up in "cluster_profile/config.yaml" file according to the caracteristics of the HPC cluster to be used (mainly, in default-resources, change the partition parameter with the right partition name of the cluster to be used).  
Eventually, custom cluster parameters "resources" in some rules of the snakefile if needed.  

At first, it is recommended to make a dry-run (code test without execution) of the analysis.  
This will check all the rules and the parameters in the smk_config.yaml file and print all the command lines which would have been executed.  

```console
snakemake -nrp --singularity-args '--bind /my_genomeFasta_path' --profile cluster_profile/
```

## Run CLARI-TE_smk  

To launch CLARI-TE_smk, it is recommanded to open a screen or launch it in a srun command.  

Screen commands reminder:
```console
screen -S "name"    #create a screen
screen -list        #list existing screens
screen ctrl-a + ctrl-d   #detach a screen
screen -r "name"    #reattache a screen
```

Adapt the "bind" singularity argument to the path you need to mount into the singularity container, in order to make the folder containing genome fasta reachable if not in your $HOME.

```console
module load gcc/8.1.0 python/3.7.1 snakemake/7.15.1  
snakemake --singularity-args '--bind /my_genomeFasta_path' --profile cluster_profile/
```

## Pipeline graphical representation  

To have a view of the workflow and its successive rules:  
```console
snakemake --rulegraph > rulegraph.dot
```
The .dot file can be converted in .png with [Graphviz](https://dreampuf.github.io/GraphvizOnline)  

Flowchart before checkpoint rule:  
![rulegraph_before](images/rulegraph_before_checkpoint.png)

Flowchart after checkpoint rule, i.e. chunks_fasta, obtained after the pipeline has finished:  
![rulegraph_after](images/rulegraph_after_checkpoint.png)

To visualize the diagram of the parallelized processes, run the following command:  
```console
snakemake --dag > dag.dot
```
Example of a diagram at pipeline start, without RepeatMasker and ClariTE rules, as the checkpoint rule is not passed yet:  
![dag](images/dag_before_checkpoint.png)

The diagram after checkpoint can be obtained at the end, but is not really readable due to hundreds of parallel sub jobs.  

## About Singularity image  

CLARI-TE_smk uses a Singularity image named 'singularity_RMclariTE.sif' containing a Conda environment, and tools (crossmatch, CLARI-TE) for TEs annotation (built with Singularity v3.8.5).  
This Snakemake pipeline is configured to execute all Snakemake rules within this image.  
It is not recommanded to change the Singularity image, but if necessary, the definition file to built it is 'singularity_RMclariTE.def'.  
After possible changes, following commands can be used to re-build the image (with root permission) :  

```console
## to see the configuration file Singularity.conf with --dry-run option:
sudo singularity config global --dry-run --set "mount hostfs" yes
## to turn on that singularity will probe for all mounted file systems that are mounted on the host, and bind those into the container at runtime
sudo singularity config global --set "mount hostfs" yes
## to check that it is effective
sudo singularity config global --get "mount hostfs"

## to build the image with sudo permissions
sudo singularity build singularity_RMclariTE.sif singularity_RMclariTE.def

## to run the image as a user in an interactive way, and thus having access to its environment and tools
singularity shell singularity_RMclariTE.sif

## to execute a command within the image as a user in an non interactive way
singularity exec singularity_RMclariTE.sif RepeatMasker --help

## to push it on gilab registry ($SINGULARITY_DOCKER_PASSWORD is the gitlab user access token permitting push/pull image to registry)  
singularity push --docker-username $SINGULARITY_DOCKER_USER --docker-password $SINGULARITY_DOCKER_PASSWORD RMclariTE.sif oras://registry.forgemia.inra.fr/umr-gdec/clari-te_smk
```

## Application example  

Transposable elements annotation of *Aegilops Umbellulata* genome ([Singh et al, 2024](https://www.biorxiv.org/content/10.1101/2024.01.13.575480v1)), in collaboration with U. Gill (North Dakota State University, Fargo, USA).  

![Table](images/table.png)  

## Support  

Pauline LASSERRE-ZUBER  
Bioinformatic Engineer  
pauline.lasserre-zuber@inrae.fr  
UMR INRAE UCA 1095 [GDEC](https://umr1095.clermont.hub.inrae.fr/)  

## Authors and acknowledgment  

Pauline LASSERRE-ZUBER (INRAe), Josquin DARON (Institut Pasteur), Frederic CHOULET (INRAe)  
